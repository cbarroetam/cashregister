/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.test;

import com.rocketmiles.cashregister.bussiness.CashRegister;
import com.rocketmiles.cashregister.bussiness.CashRegisterImpl;
import com.rocketmiles.cashregister.model.BillDenominationType;
import com.rocketmiles.cashregister.model.Money;
import com.rocketmiles.cashregister.model.MoneyVault;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test Class CashRegisterTest
 *
 * @author Carlos D. Barroeta M.
 */
public class CashRegisterTest {

    private CashRegister cashRegister;

    @Before
    public void setup() {
        cashRegister = new CashRegisterImpl(new MoneyVault());
    }

    //--------------------------------------------------------------------------
    // Test Case scenarios
    //--------------------------------------------------------------------------
    /**
     * Simple test case scenario according to the code challenge specification
     * document.
     */
    @Test(expected = IllegalStateException.class)
    public void testSimpleScenarioFromCodeChallenge() {

        System.out.println("============ testSimpleScenarioFromCodeChallenge =====================");

        System.out.println(cashRegister.show());

        // lets put $68 dollars into the Cash Register
        int currentTotal = cashRegister.put(new Money(BillDenominationType.TWENTY, 1),
                new Money(BillDenominationType.TEN, 2),
                new Money(BillDenominationType.FIVE, 3),
                new Money(BillDenominationType.TWO, 4),
                new Money(BillDenominationType.ONE, 5));

        Assert.assertEquals(68, currentTotal);

        System.out.println(cashRegister.show());

        // lets put $60 
        currentTotal = cashRegister.put(new Money(BillDenominationType.TWENTY, 1),
                new Money(BillDenominationType.TEN, 2),
                new Money(BillDenominationType.FIVE, 3),
                new Money(BillDenominationType.TWO, 0),
                new Money(BillDenominationType.ONE, 5));

        Assert.assertEquals(128, currentTotal);

        System.out.println(cashRegister.show());

        // lets take $75
        currentTotal = cashRegister.take(new Money(BillDenominationType.TWENTY, 1),
                new Money(BillDenominationType.TEN, 4),
                new Money(BillDenominationType.FIVE, 3),
                new Money(BillDenominationType.TWO, 0),
                new Money(BillDenominationType.ONE, 10));

        Assert.assertEquals(43, currentTotal);

        System.out.println(cashRegister.show());

        // give away 11 on change
        cashRegister.change(11);

        currentTotal -= 11;

        Assert.assertEquals(32, currentTotal);

        System.out.println(cashRegister.show());

        cashRegister.change(14);
    }
}
