/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.test;

import com.rocketmiles.cashregister.model.BillDenominationType;
import com.rocketmiles.cashregister.model.Money;
import com.rocketmiles.cashregister.model.MoneyVault;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test Class MoneyVaultTest
 *
 * @author
 */
public class MoneyVaultTest {

    // System Under Test
    private final MoneyVault moneyVaultSUT = new MoneyVault();

    //--------------------------------------------------------------------------
    // Data Builder API
    //--------------------------------------------------------------------------
    /**
     *
     * @return
     */
    public Money[] createPersonalWallet() {
        return new Money[]{
            new Money(BillDenominationType.ONE, 20),
            new Money(BillDenominationType.TWENTY, 2),
            new Money(BillDenominationType.FIVE, 1)};
    }

    /**
     *
     * @return
     */
    public int getCurrentTotalOnPersonalWallet() {
        int total = 0;

        for (Money moneyInstance : createPersonalWallet()) {
            total += moneyInstance.getTotal();
        }

        return total;
    }

    //--------------------------------------------------------------------------
    // Test Case scenarios
    //--------------------------------------------------------------------------
    /**
     *
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCreateMoneyInstanceWithInvalidDenomination() {
        Money money = new Money(null, -1);
    }

    /**
     *
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCreateMoneyInstanceWithInvalidBillQuantity() {
        Money money = new Money(BillDenominationType.FIVE, -1);
    }

    /**
     * Simple scenario to add
     */
    @Test()
    public void testAddMoneyToMyVault() {

        for (Money moneyInstance : createPersonalWallet()) {
            moneyVaultSUT.addMoney(moneyInstance);
        }

        Assert.assertEquals(getCurrentTotalOnPersonalWallet(),
                moneyVaultSUT.getCurrentCashAvailable());
    }

    /**
     *
     */
    @Test()
    public void testRemoveMoneyFromMyVault() {

        for (Money moneyInstance : createPersonalWallet()) {
            moneyVaultSUT.addMoney(moneyInstance);
        }

        for (Money moneyInstance : createPersonalWallet()) {
            moneyVaultSUT.removeMoney(moneyInstance);
        }

        Assert.assertEquals(0, moneyVaultSUT.getCurrentCashAvailable());

        Assert.assertFalse(moneyVaultSUT.hasFunds());
    }
}
