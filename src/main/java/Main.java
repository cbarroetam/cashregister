/**
 * Copyright (C) 2016 - All Rights Reserved
 */
import com.rocketmiles.cashregister.bussiness.CashRegister;
import com.rocketmiles.cashregister.bussiness.CashRegisterImpl;
import com.rocketmiles.cashregister.command.AbstractCommandLine;
import com.rocketmiles.cashregister.model.MoneyVault;
import com.rocketmiles.cashregister.utils.ApplicationMessages;
import java.util.Scanner;

/**
 * Class Main
 *
 * Entry point for this application
 *
 * @author Carlos D. Barroeta M.
 */
public class Main {

    /**
     * 
     * @return 
     */
    public static String readInputLine() {
        String input;
        if (System.console() != null) {
            input = System.console().readLine();
        } else {
            Scanner scanner = new Scanner(System.in);
            input = scanner.nextLine();
        }
        return input;
    }

    /**
     * Runs the main thread for this application.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println(ApplicationMessages.READY_CLI_MSG);

        CashRegister cashRegister = new CashRegisterImpl(new MoneyVault());

        while (true) {

            System.out.print("> ");

            String input = readInputLine();

            String[] commandAndArgs = {};

            if (input != null) {
                commandAndArgs = input.split(" ");
            }

            try {
                AbstractCommandLine command = AbstractCommandLine.getCommand(cashRegister, commandAndArgs);

                command.execute(commandAndArgs);
                
            } catch (Throwable th) {

                System.err.println(th.getMessage());
            }
        }
    }

}
