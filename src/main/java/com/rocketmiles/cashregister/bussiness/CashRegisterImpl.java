/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.bussiness;

import com.rocketmiles.cashregister.model.Money;
import com.rocketmiles.cashregister.model.MoneyVault;
import com.rocketmiles.cashregister.utils.ApplicationMessages;
import java.util.ArrayList;
import java.util.List;

/**
 * Class CashRegisterImpl
 *
 * Concrete implementation of the interface CashRegister
 *
 * @author Carlos D. Barroeta M.
 */
public class CashRegisterImpl implements CashRegister {

    private final MoneyVault vault;

    /**
     * Convenience method to check the current state from the vault before
     * taking money from the vault.
     *
     * @param moneyInstances
     * @throws IllegalStateException
     */
    private void checkVaultState(Money[] moneyInstances) throws IllegalStateException {
        if (!vault.hasFunds()) {
            throw new IllegalStateException(ApplicationMessages.NO_FUNDS_EXCEPTION_MSG);
        }

        int totalRequested = 0;
        for (Money moneyInstance : moneyInstances) {
            totalRequested += moneyInstance.getTotal();
        }

        if (totalRequested > vault.getCurrentCashAvailable()) {
            throw new IllegalStateException(ApplicationMessages.AMOUNT_EXCEEDED_EXCEPTION_MSG);
        }
    }

    /**
     *
     * @param aMoneyVault
     * @param amountToGiveOnChange
     * @return
     */
    private boolean isPossibleToGetChangeFromThisMoney(Money aMoneyVault,
            Money lookAheadMoney, int amountToGiveOnChange, boolean thereAreOtherDenominations) {

        if (aMoneyVault.getTotal() > 0) {
            if (aMoneyVault.getDenomination().getValue() <= amountToGiveOnChange) {

                if (aMoneyVault.getDenomination().getValue() + lookAheadMoney.getDenomination().getValue() <= amountToGiveOnChange) {

                    return Boolean.TRUE;
                } else if (aMoneyVault.getDenomination().equals(lookAheadMoney.getDenomination())
                        && lookAheadMoney.getBills() == aMoneyVault.getBills()) {
                    return Boolean.TRUE;
                } else if (thereAreOtherDenominations) {
                    return Boolean.TRUE;
                }
            }
        }

        return Boolean.FALSE;
    }

    /**
     * Given a ascendant ordered collection of Money elements, this method will
     * perform the calculation of which denominations can be used to give the
     * change spcified by the parameter amountToGiveOnChange
     *
     * @param aMoneyVault
     * @param amountToGiveOnChange
     * @return
     */
    private List<Money> getPossibleBillsUsedToGiveChange(List<Money> aMoneyVault, int amountToGiveOnChange) {

        int vaultIdx = aMoneyVault.size() - 1;

        ArrayList<Money> moneyUsed = new ArrayList<>();

        while (amountToGiveOnChange > 0 && vaultIdx >= 0) {

            Money currentMoney = aMoneyVault.get(vaultIdx);

            Money lookAheadMoney = vaultIdx > 0 ? aMoneyVault.get(vaultIdx - 1) : aMoneyVault.get(vaultIdx);

            if (isPossibleToGetChangeFromThisMoney(currentMoney, lookAheadMoney, amountToGiveOnChange, (vaultIdx - 1 > 0))) {

                if (moneyUsed.contains(currentMoney)) {
                    Money moneyAlreadyUsed = moneyUsed.get(moneyUsed.indexOf(currentMoney));
                    moneyAlreadyUsed.addBills(1);
                } else {
                    moneyUsed.add(new Money(currentMoney.getDenomination(), 1));
                }

                currentMoney.removeBills(1);

                amountToGiveOnChange = amountToGiveOnChange - currentMoney.getDenomination().getValue();

            } else {
                vaultIdx--;
            }
        }

        return moneyUsed;
    }

    /**
     *
     * @param moneyUsed
     * @param amount
     */
    private void takeMoneyFromTheVault(List<Money> moneyUsed, int amount) {
        int totalMoneyUsed = 0;
        // lets check if we can give away the money
        totalMoneyUsed = moneyUsed.stream().map((money) -> money.getTotal()).reduce(totalMoneyUsed, Integer::sum);

        if (totalMoneyUsed == amount) {
            for (Money money : moneyUsed) {
                take(money);
            }
        } else {
            throw new IllegalStateException(ApplicationMessages.CHANGE_NOT_AVAILABLE_EXCEPTION_MSG);
        }
    }

    /**
     * Performs a simple check to see if we've got the change using all the
     * bills from one or many denominations
     *
     * @param changeAmount amount to be given on change
     *
     * @return a collection with the denominations used to give the change
     */
    private List<Money> getChangeFromCompleteDenomination(List<Money> vaultOrderedAsc, int changeAmount) {

        int totalMoneyUsed = 0;

        List<Money> moneyUsed = new ArrayList<>();

        for (Money moneyInstance : vaultOrderedAsc) {
            if (moneyInstance.getTotal() == changeAmount || totalMoneyUsed == changeAmount) {
                moneyUsed.add(moneyInstance);
                break;
            } else {
                totalMoneyUsed += moneyInstance.getTotal();
            }
        }

        return moneyUsed;
    }

    /**
     *
     * @param vault
     */
    public CashRegisterImpl(MoneyVault vault) {
        this.vault = vault;
    }

    //--------------------------------------------------------------------------
    // Methods implemented from the interface CashRegister
    //--------------------------------------------------------------------------
    /**
     * @see CashRegister#show()
     */
    @Override
    public String show() {
        return vault.toString();
    }

    /**
     * @see CashRegister#put(com.rocketmilles.cashregister.model.Money...)
     */
    @Override
    public int put(Money... moneyInstances) throws IllegalArgumentException {

        for (Money moneyInstance : moneyInstances) {
            if (moneyInstance == null) {
                throw new IllegalArgumentException("Money instance must not be null or an empty value");
            }

            vault.addMoney(moneyInstance);
        }

        return vault.getCurrentCashAvailable();
    }

    /**
     * @see CashRegister#take(com.rocketmilles.cashregister.model.Money...)
     */
    @Override
    public int take(Money... moneyInstances) throws IllegalArgumentException, IllegalStateException {

        checkVaultState(moneyInstances);

        for (Money moneyInstance : moneyInstances) {
            if (moneyInstance == null) {
                throw new IllegalArgumentException("Money instance must not be null or an empty value");
            }

            vault.removeMoney(moneyInstance);
        }

        return vault.getCurrentCashAvailable();
    }

    /**
     * @see CashRegister#change(int)
     */
    @Override
    public void change(int amount) {

        if (!vault.hasFunds()) {
            throw new IllegalStateException(ApplicationMessages.NO_FUNDS_EXCEPTION_MSG);
        }

        if (amount > vault.getCurrentCashAvailable()) {
            throw new IllegalStateException(ApplicationMessages.AMOUNT_EXCEEDED_EXCEPTION_MSG);
        }

        List<Money> vaultOrderedAsc = vault.getAvailableMoneyOnAscendantOrder();

        List<Money> moneyUsed = getChangeFromCompleteDenomination(vaultOrderedAsc, amount);

        if (moneyUsed.isEmpty()) {
            moneyUsed = getPossibleBillsUsedToGiveChange(vaultOrderedAsc, amount);
        }

        takeMoneyFromTheVault(moneyUsed, amount);
    }
}
