/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.bussiness;

import com.rocketmiles.cashregister.model.Money;

/**
 * Interface CashRegister
 *
 * This contract defines the operations related to administrate and manage a
 * simple money vault.
 *
 * @author Carlos D. Barroeta M.
 */
public interface CashRegister {

    /**
     * Shows current state in total and each denomination on the vault.
     *
     * @return a String representation of the vault that is under control.
     */
    String show();

    /**
     * Put bills in each denomination.
     *
     * @param moneyInstances bills on any denomination to be added to an
     *                       existing vault.
     *
     * @return the current amount of cash available after the execution of this
     *         operation
     *
     * @throws IllegalArgumentException if any of the arguments received is a
     *                                  null value
     */
    int put(Money... moneyInstances) throws IllegalArgumentException;

    /**
     * Take bills in each denomination
     *
     * @param moneyInstances bills on any denomination to be removed from an
     *                       existing vault.
     *
     * @return the current amount of cash available after the execution of this
     *         operation
     *
     * @throws IllegalArgumentException if any of the arguments received is a
     *                                  null value
     *
     * @throws IllegalStateException    in case the current operation cannot be
     *                                  executed.
     *
     */
    int take(Money... moneyInstances) throws IllegalArgumentException, IllegalStateException;

    /**
     * Given the amount remove money from cash if the vault has the correct
     * state to accomplish the operation register
     *
     * @param amount total of money to extract from the vault
     */
    void change(int amount);
}
