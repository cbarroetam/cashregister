/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.model;

import com.rocketmiles.cashregister.utils.ApplicationMessages;
import java.io.Serializable;
import java.util.Objects;

/**
 * Model Class Money
 *
 * Simple wrapper class to represent a denomination with the total of bills
 * available for the specific denomination type.
 *
 * @author Carlos D. Barroeta M.
 */
public class Money implements Serializable {

    private BillDenominationType denomination;

    private int bills;

    /**
     * Constructs a new instance of type <code>Money</code> with the specific
     * denomination and bill quantity.
     *
     * @param denomination specific denomination.
     * @param quantity     total of bills from this denomination
     *
     * @throws IllegalArgumentException in case of the denomination is a null
     *                                  value or the quantity of bills is a
     *                                  negative number
     */
    public Money(BillDenominationType denomination, int quantity) throws IllegalArgumentException {
        if (denomination == null) {
            throw new IllegalArgumentException("Denomination type cannot be a null value");
        }
        if (quantity < 0) {
            throw new IllegalArgumentException("Total bills from this denomination must be a non negative number");
        }

        this.denomination = denomination;

        this.bills = quantity;
    }

    /**
     * Gets the denomination type for this Money wrapper instance.
     *
     * @return denomination type of this money wrapper instance
     */
    public BillDenominationType getDenomination() {
        return denomination;
    }

    /**
     * Gets the total of bills for this Money wrapper instance.
     *
     * @return total of bills available on this instance.
     */
    public int getBills() {
        return bills;
    }

    /**
     * Convinience method to substract one bill from this money wrapper.
     * @param billsToRemove
     */
    public void removeBills(int billsToRemove) {
        if (bills > 0 && billsToRemove > 0) {
            bills -= billsToRemove;
        }
    }

    /**
     * Convinience method to add more bills to this money wrapper.
     *
     * @param newBills total of bills to add.
     */
    public void addBills(int newBills) {
        if (newBills > 0) {
            bills += newBills;
        }
    }

    /**
     * Gets the monetary value of this wrapper. This value is computed after
     * multiply the current denomination by the total of bills available.
     *
     * @return monetary value for this wrapper instance.
     */
    public int getTotal() {
        return bills * denomination.getValue();
    }

    //--------------------------------------------------------------------------
    // Methods overrided from Object Class
    //--------------------------------------------------------------------------
    /**
     * Gets a <code>String</code> representation for this wrapper.
     *
     * @return a simple String representation $10x1=$10
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(ApplicationMessages.CURRENCY).
                append(getDenomination().getValue()).
                append("x").append(getBills()).
                append("= ").
                append(ApplicationMessages.CURRENCY).append(getTotal());

        return sb.toString();
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.denomination);
        return hash;
    }

    /**
     * Two instance of this kind will be equals if both represent the same
     * denomination. It will be omited the total of bills on each instances.
     *
     * @param obj an instance of type <code>Money</code>
     *
     * @return true if both wrappers represents the same denomination, false
     *         otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Money)) {
            return Boolean.FALSE;
        }

        Money other = (Money) obj;

        return other.denomination.equals(this.denomination);
    }
}
