/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.model;

import com.rocketmiles.cashregister.utils.ApplicationMessages;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Model Class MoneyVault
 *
 * Wrapper class that represents a simple money vault or wallet. This wrapper
 * holds the money ordered in descendant way according to the bill's
 * denomination.
 *
 * @author Carlos D. Barroeta M.
 */
public class MoneyVault implements Serializable {

    private final ArrayList<Money> descOrderedVault;

    private int currentCashAvailable;

    /**
     * Sorting comparator to order the bills from higher to lower based on the
     * denomination
     */
    private final Comparator<Money> DESC = new Comparator<Money>() {
        @Override
        public int compare(Money o1, Money o2) {
            if (o1.getDenomination().getValue() > o2.getDenomination().getValue()) {
                return -1;
            } else if (o1.getDenomination().getValue() < o2.getDenomination().getValue()) {
                return 1;
            }
            return 0;
        }
    };

    /**
     * Sorting comparator to order the bills from lower to higher based on the
     * denomination
     */
    private final Comparator<Money> ASC = new Comparator<Money>() {
        @Override
        public int compare(Money o1, Money o2) {
            if (o1.getDenomination().getValue() > o2.getDenomination().getValue()) {
                return 1;
            } else if (o1.getDenomination().getValue() < o2.getDenomination().getValue()) {
                return -1;
            }
            return 0;
        }
    };

    /**
     * Constructs a new instance of <code>MoneyVault</code>
     */
    public MoneyVault() {
        descOrderedVault = new ArrayList();

        currentCashAvailable = 0;
    }

    /**
     * Adds money to this vault.
     *
     * @param money total of bills from the specific denomination to be added to
     *              the vault.
     */
    public void addMoney(Money money) {

        int indexOnVault = descOrderedVault.indexOf(money);

        if (indexOnVault >= 0) {
            Money moneyOnVault = descOrderedVault.get(indexOnVault);

            moneyOnVault.addBills(money.getBills());
        } else {
            descOrderedVault.add(money);
        }

        currentCashAvailable += money.getTotal();
    }

    /**
     * Removes money from this vault only if the current cash is available.
     *
     * @param money total of bills from the specific denomination to be removed
     *              from the vault.
     */
    public void removeMoney(Money money) {

        if (hasFunds()) {
            int indexOnVault = descOrderedVault.indexOf(money);

            if (indexOnVault >= 0) {
                Money moneyOnVault = descOrderedVault.get(indexOnVault);

                moneyOnVault.removeBills(money.getBills());
            } else {
                descOrderedVault.add(money);
            }

            currentCashAvailable -= money.getTotal();
        }
    }

    /**
     * Convenience method to get a representation from this vault in ascendant
     * order, based on the denominations which current total of bills, are
     * greater than zero.
     *
     * @return a representation in ascendant order from this vault.
     */
    public List<Money> getAvailableMoneyOnAscendantOrder() {

        ArrayList<Money> ascendantVault = new ArrayList<>();

        Money[] toArray = descOrderedVault.toArray(new Money[]{});

        Arrays.sort(toArray, ASC);

        // give away a clone
        for (Money moneyInstance : toArray) {
            if (moneyInstance.getBills() > 0) {
                ascendantVault.add(new Money(moneyInstance.getDenomination(), moneyInstance.getBills()));
            }
        }

        return ascendantVault;
    }

    /**
     * Gets if this vault currently has funds, base on the current cash
     * available.
     *
     * @return true if current cash if greater than zero, false otherwise
     */
    public boolean hasFunds() {
        return currentCashAvailable > 0;
    }

    /**
     * Gets the current available cash on this vault.
     *
     * @return the value on cash holded by this vault.
     */
    public int getCurrentCashAvailable() {
        return currentCashAvailable;
    }

    //--------------------------------------------------------------------------
    // Methods overrided from Object Class
    //--------------------------------------------------------------------------
    /**
     * Gets a representation of this vault in the format:$68 1 2 3 4 5 where
     * every single digit after the current cash available represets the total
     * of bills for each denomination ordered in a descendant form.
     *
     * @return string representing the current state of this vault.
     */
    @Override
    public String toString() {

        Money[] toArray = descOrderedVault.toArray(new Money[]{});

        Arrays.sort(toArray, DESC);

        StringBuilder sb = new StringBuilder(ApplicationMessages.CURRENCY);
        sb.append(currentCashAvailable).append(" ");
        
        // give away a clone
        for (Money moneyInstance : toArray) {
            sb.append(moneyInstance.getBills()).append(" ");
        }

        return sb.toString();
    }
}
