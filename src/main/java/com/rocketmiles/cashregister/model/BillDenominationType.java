/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.model;

import com.rocketmiles.cashregister.utils.ApplicationMessages;

/**
 * Enumeration Type BillDenominationType
 *
 * Simple enumeration type to represent the available denominations to be used
 * on this application.
 *
 * @author Carlos D. Barroeta M.
 */
public enum BillDenominationType {

    TWENTY(20),
    TEN(10),
    FIVE(5),
    TWO(2),
    ONE(1);

    /**
     * Value for this denomination
     */
    private final int value;

    /**
     * Constructs a new instance of <code>BillDenominationType</code> with the
     * specific value.
     *
     * @param value monetary value of this denomination.
     */
    private BillDenominationType(int value) {
        this.value = value;
    }

    /**
     * Gets the monetary value for this denomination.
     *
     * @return numeric value for this denomination
     */
    public int getValue() {
        return value;
    }

    //--------------------------------------------------------------------------
    // Methods overrided from Object Class
    //--------------------------------------------------------------------------
    
    /**
     * Retrieves a String representation for this denomination.
     *
     * @return currency symbol with the value for this denomination.
     */
    @Override
    public String toString() {
        return ApplicationMessages.CURRENCY + String.valueOf(value);
    }
}
