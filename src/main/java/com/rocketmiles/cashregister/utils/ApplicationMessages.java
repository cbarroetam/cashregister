/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.utils;

/**
 * Interface ApplicationMessages
 *
 * Convenience wrapper contract for all the constants spread around the
 * application.
 *
 * @author Carlos D. Barroeta M.
 */
public interface ApplicationMessages {

    /**
     * Default currency symbol
     */
    String CURRENCY = "$";

    /**
     * Command line message to show on-line state
     */
    String READY_CLI_MSG = "ready";

    /**
     * No funds on vault.
     */
    String NO_FUNDS_EXCEPTION_MSG = "No funds available.";

    /**
     * Vault does not have the requested money
     */
    String AMOUNT_EXCEEDED_EXCEPTION_MSG = "There is insufficient funds to accomplish this operation.";

    /**
     * Change not available on vault.
     */
    String CHANGE_NOT_AVAILABLE_EXCEPTION_MSG = "Sorry no change can be given.";

    /**
     * 
     */
    String PUT_COMMAND_NAME = "put";

    /**
     *
     */
    String SHOW_COMMAND_NAME = "show";

    /**
     * 
     */
    String TAKE_COMMAND_NAME = "take";

    /**
     * 
     */
    String CHANGE_COMMAND_NAME = "change";

    /**
     * 
     */
    String INVALID_COMMAND_NAME = "invalid";

    /**
     * 
     */
    String QUIT_COMMAND_NAME = "quit";

}
