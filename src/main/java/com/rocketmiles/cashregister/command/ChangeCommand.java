/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.command;

import com.rocketmiles.cashregister.bussiness.CashRegister;

/**
 * Command Class ChangeCommand
 *
 * @author Carlos D. Barroeta M.
 */
public class ChangeCommand extends AbstractCommandLine {

    public ChangeCommand(CashRegister instance) {
        super(instance);
    }

    @Override
    public void execute(String... args) {

        try {
            if (args.length == 2) {
                cashRegister.change(Integer.parseInt(args[1]));

                System.out.println(cashRegister.show());
            } else {
                System.err.println("This commands only admits one parameter");
            }

        } catch (NumberFormatException nfex) {

            System.err.println("Bad arguments in command. Only numbers are allowed.");
        }
    }
}
