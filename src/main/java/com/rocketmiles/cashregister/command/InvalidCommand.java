/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.command;

import com.rocketmiles.cashregister.bussiness.CashRegister;

/**
 * Command Class InvalidCommand
 *
 * @author Carlos D. Barroeta M.
 */
public class InvalidCommand extends AbstractCommandLine {

    public InvalidCommand(CashRegister instance) {
        super(instance);
    }

    @Override
    public void execute(String... args) {
        System.err.println("Enter a valid comand.");
    }
}
