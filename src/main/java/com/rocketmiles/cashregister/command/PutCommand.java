/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.command;

import com.rocketmiles.cashregister.bussiness.CashRegister;
import com.rocketmiles.cashregister.model.BillDenominationType;
import com.rocketmiles.cashregister.model.Money;

/**
 * Command Class PutCommand
 *
 * @author Carlos D. Barroeta M.
 */
public class PutCommand extends AbstractCommandLine {

    public PutCommand(CashRegister instance) {
        super(instance);
    }

    @Override
    public void execute(String... args) {

        try {
            if (args.length == 6) {
                cashRegister.put(
                        new Money(BillDenominationType.TWENTY, Integer.parseInt(args[1])),
                        new Money(BillDenominationType.TEN, Integer.parseInt(args[2])),
                        new Money(BillDenominationType.FIVE, Integer.parseInt(args[3])),
                        new Money(BillDenominationType.TWO, Integer.parseInt(args[4])),
                        new Money(BillDenominationType.ONE, Integer.parseInt(args[5])));

                System.out.println(cashRegister.show());
            } else {
                System.err.println("You must provided a value for each denomination");
            }

        } catch (NumberFormatException nfex) {

            System.err.println("Bad arguments in command. Only numbers are allowed.");
        }
    }
}
