/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.command;

import com.rocketmiles.cashregister.bussiness.CashRegister;

/**
 * Command Class ShowCommand
 *
 * @author Carlos D. Barroeta M.
 */
public class ShowCommand extends AbstractCommandLine {

    public ShowCommand(CashRegister instance) {
        super(instance);
    }

    /**
     *
     * @param args
     */
    @Override
    public void execute(String... args) {
        System.out.println(cashRegister.show());
    }
}
