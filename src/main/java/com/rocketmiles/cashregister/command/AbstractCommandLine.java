/**
 * Copyright (C) 2016 - All Rights Reserved
 */
package com.rocketmiles.cashregister.command;

import com.rocketmiles.cashregister.bussiness.CashRegister;
import com.rocketmiles.cashregister.utils.ApplicationMessages;
import java.util.HashMap;

/**
 * Abstract Class AbstractCommandLine
 *
 * Base class to represent the available command line supported by this
 * application
 *
 * @author Carlos D. Barroeta M.
 */
public abstract class AbstractCommandLine {

    protected CashRegister cashRegister;

    /**
     * Creates an instance of this type with the specific CashRegister to
     * execute operations on a given money's vault.
     *
     * @param cashRegister
     */
    public AbstractCommandLine(CashRegister cashRegister) {
        this.cashRegister = cashRegister;
    }

    /**
     * Retrieves an instance of the command to be executed, based on the name of
     * the command, that is, the element at the zero index from the String array
     * received as parameter. In addition to the command name and its arguments,
     * is required for this method and instance of the specific
     * {@link CashRegister} that will performing the operations related to the
     * administration of the Money Vault.
     *
     * @param cRegister instance responsible to administrate the virtual vault.
     * @param args      command name and its arguments.
     *
     * @return a concrete implementation of the command to be executed based on
     *         the command name
     */
    public static AbstractCommandLine getCommand(CashRegister cRegister, String... args) {
        switch (args[0]) {
            case ApplicationMessages.QUIT_COMMAND_NAME:
                System.out.println("Bye");
                System.exit(0);
                break;
                
            case ApplicationMessages.SHOW_COMMAND_NAME:
                return new ShowCommand(cRegister);

            case ApplicationMessages.PUT_COMMAND_NAME:
                return new PutCommand(cRegister);

            case ApplicationMessages.TAKE_COMMAND_NAME:
                return new TakeCommand(cRegister);

            case ApplicationMessages.CHANGE_COMMAND_NAME:
                return new ChangeCommand(cRegister);
        }

        return new InvalidCommand(cRegister);
    }

    /**
     * Entry point for the execution of administrative opearations using the
     * {@link CashRegister} associated to the command instance.
     *
     * @param args arguments and command name.
     */
    public abstract void execute(String... args);
}
